package cern.ais.gridwars;

/**
 * Created by ��������� on 03.11.2015.
 */
import cern.ais.gridwars.bot.PlayerBot;
import cern.ais.gridwars.command.MovementCommand;

import java.util.List;




public class NiceBot implements PlayerBot {
    public NiceBot() {}

    private static void bubble_srt(int array[]) {
        int n = array.length;
        int k;
        for (int m = n; m >= 0; m--) {
            for (int i = 0; i < n - 1; i++) {
                k = i + 1;
                if (array[i] > array[k]) {
                    swapNumbers(i, k, array);
                }
            }
        }
    }

    private static void swapNumbers(int i, int j, int[] array) {
        int temp;
        temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    @Override public void getNextCommands(UniverseView universeView, List<MovementCommand> movementCommands) {
        long turnNumber = universeView.getCurrentTurn();
        List<Coordinates> myCells = universeView.getMyCells();

        if (turnNumber < 3) {
            long population = universeView.getPopulation(myCells.get(0));
            movementCommands.add(new MovementCommand(myCells.get(0), MovementCommand.Direction.UP, population / 5));
            movementCommands.add(new MovementCommand(myCells.get(0), MovementCommand.Direction.DOWN, population / 5));
            movementCommands.add(new MovementCommand(myCells.get(0), MovementCommand.Direction.RIGHT, population / 5));
            movementCommands.add(new MovementCommand(myCells.get(0), MovementCommand.Direction.LEFT, population / 5));
        }
        else if (turnNumber < 30){
            int size = myCells.size();
            int[] y = new int[size];
            int[] x = new int[size];

            for (int i=0; i<size; i++) {
                x[i] = myCells.get(i).getX();
                y[i] = myCells.get(i).getY();
            }
            bubble_srt(x);
            bubble_srt(y);

            for (Coordinates cell : myCells) {
                long population = universeView.getPopulation(cell);
                if (cell.getY() == y[0]) {
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.UP, population));
                }
                else if (cell.getY() == y[4]) {
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.DOWN, population));
                }
                else if (cell.getX() == x[0]) {
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.LEFT, population));
                }
                else if (cell.getX() == x[4]) {
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.RIGHT, population));
                }
                else {
                    /*movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.LEFT, population / 5));
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.RIGHT, population / 5));
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.UP, population / 5));
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.DOWN, population / 5));*/
                }
            }
        }
        else {
            for (Coordinates cell : myCells) {
                long population = universeView.getPopulation(cell);
                if (population > 5) {
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.LEFT, population / 5));
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.RIGHT, population / 5));
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.UP, population / 5));
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.DOWN, population / 5));
                }
            }
        }
    }
}

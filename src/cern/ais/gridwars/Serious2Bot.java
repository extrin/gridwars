package cern.ais.gridwars;

/**
 * Created by ��������� on 03.11.2015.
 */

import cern.ais.gridwars.bot.PlayerBot;
import cern.ais.gridwars.command.MovementCommand;

import java.util.List;

public class Serious2Bot implements PlayerBot {
    public Serious2Bot() {}

    @Override public void getNextCommands(UniverseView universeView, List<MovementCommand> movementCommands) {
        List<Coordinates> myCells = universeView.getMyCells();

        int cellsNumber = myCells.size();
        if (cellsNumber < 10) {
            int i = 0;
            for (Coordinates cell : myCells) {
                i++;
                long population = universeView.getPopulation(cell);
                if (i % 2 ==0) {
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.RIGHT, population / 2));
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.LEFT, population / 2));
                }
                else {
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.UP, population / 2));
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.DOWN, population / 2));
                }
            }
        }
        else {
            for (Coordinates cell : myCells) {
                long population = universeView.getPopulation(cell);
                if (population > 5) {
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.LEFT, population / 5));
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.RIGHT, population / 5));
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.UP, population / 5));
                    movementCommands.add(new MovementCommand(cell, MovementCommand.Direction.DOWN, population / 5));
                }
            }
        }
    }
}

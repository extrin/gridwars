package cern.ais.gridwars;

/**
 * Created by ��������� on 02.11.2015.
 */

import cern.ais.gridwars.bot.PlayerBot;
import cern.ais.gridwars.command.MovementCommand;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SeriousBot implements PlayerBot {

    public SeriousBot() {}

    private MovementCommand.Direction whichDirection (Coordinates fromCell, Coordinates toCell) {
        if (toCell.getX() > fromCell.getX() && toCell.getY() == fromCell.getY()) {
            return MovementCommand.Direction.RIGHT;
        }
        else if (toCell.getX() < fromCell.getX() && toCell.getY() == fromCell.getY()) {
            return MovementCommand.Direction.LEFT;
        }
        else if (toCell.getX() == fromCell.getX() && toCell.getY() > fromCell.getY()) {
            return MovementCommand.Direction.UP;
        }
        else if (toCell.getX() == fromCell.getX() && toCell.getY() < fromCell.getY()) {
            return MovementCommand.Direction.DOWN;
        }
        else return MovementCommand.Direction.DOWN;
    }

    @Override public void getNextCommands(UniverseView universeView, List<MovementCommand> movementCommands) {
        List<Coordinates> myCells = universeView.getMyCells();

        /* ������ ���� ������� ����� ������, ����� ����������� ����������� �������� ���������� ������!*/
        int size = universeView.getUniverseSize();
        long[][] field = new long [size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                field[i][j] = 0;
            }
        }
        for (Coordinates currentCell : myCells) {
            int x = currentCell.getX();
            int y = currentCell.getY();
            field[x][y] = universeView.getPopulation(currentCell);
        }

        /*TODO
        * check if there are cells with 100 or close to 100 units
        * expand these cells*/
        for (Coordinates currentCell: myCells) {
            int x = currentCell.getX();
            int y = currentCell.getY();
            if (field[x][y] == 100 || field[x][y] * 1.1 >= 100) {
                movementCommands.add(new MovementCommand(currentCell, MovementCommand.Direction.UP, (long)20));
                field[x][y + 1] += 20;
                field[x][y] -= 20;
                movementCommands.add(new MovementCommand(currentCell, MovementCommand.Direction.LEFT, (long)20));
                field[x - 1][y] += 20;
                field[x][y] -= 20;
                movementCommands.add(new MovementCommand(currentCell, MovementCommand.Direction.RIGHT, (long)20));
                field[x + 1][y] += 20;
                field[x][y] -= 20;
                movementCommands.add(new MovementCommand(currentCell, MovementCommand.Direction.DOWN, (long)20));
                field[x][y - 1] += 20;
                field[x][y] -= 20;
            }
        }

        for (Coordinates currentCell : myCells) {
            // get neighbour cells of my current cell
            List<Coordinates> neighbourCells = new ArrayList<Coordinates>();
            neighbourCells.add(currentCell.getDown());
            neighbourCells.add(currentCell.getLeft());
            neighbourCells.add(currentCell.getRight());
            neighbourCells.add(currentCell.getUp());

            // process neighbour cells of my current cell
            for (Coordinates neighbourCell : neighbourCells) {
                int x = neighbourCell.getX();
                int y = neighbourCell.getY();
                if (universeView.isEmpty(neighbourCell) && field[x][y] == 0) {
                    // neighbour cell of my current cell is empty
                    // process neighbour cells of a neighbour cell of my current cell
                    List<Coordinates> cells = new ArrayList<Coordinates>();
                    cells.add(neighbourCell.getDown());
                    cells.add(neighbourCell.getUp());
                    cells.add(neighbourCell.getLeft());
                    cells.add(neighbourCell.getRight());

                    for (Coordinates cell : cells) {
                        x = cell.getX();
                        y = cell.getY();
                        if (!universeView.belongsToMe(cell) && field[x][y] == 0) {
                        }
                        else if (universeView.isEmpty(cell) && field[x][y] == 0) {
                                x = currentCell.getX();
                                y = currentCell.getY();
                                long currentPopulation = field[x][y];
                                if (currentPopulation > 20) {
                                    MovementCommand.Direction currentDirection = whichDirection(currentCell, neighbourCell);
                                    movementCommands.add(new MovementCommand(currentCell, currentDirection, currentPopulation / 4));
                                    field[x][y] = currentPopulation - currentPopulation / 4;
                                    x = neighbourCell.getX();
                                    y = neighbourCell.getY();
                                    field[x][y] = currentPopulation / 4;
                                }
                                // expand
                                // check than expansion troops number can increase
                        } else {
                            x = cell.getX();
                            y = cell.getY();
                            if (field[x][y] == 0) {
                                x = currentCell.getX();
                                y = currentCell.getY();
                                long enemyPopulation = universeView.getPopulation(cell);
                                long myPopulation = field[x][y];
                                double percent = universeView.getGrowthRate();
                                if ((myPopulation - 5) * (1.0 + percent) > enemyPopulation * (1.0 + percent)) {
                                    MovementCommand.Direction currentDirection = whichDirection(currentCell, neighbourCell);
                                    movementCommands.add(new MovementCommand(currentCell, currentDirection, myPopulation - 5));
                                    field[x][y] = 5;
                                    x = neighbourCell.getX();
                                    y = neighbourCell.getY();
                                    field[x][y] = myPopulation - 5;
                                }
                                // there is an enemy
                                // check if it is stronger than me im my cell
                                // if weak, expand
                                // if stronger, do nothing
                            }
                        }
                    }
                }
            else {
                    x = neighbourCell.getX();
                    y = neighbourCell.getY();
                    if (!universeView.belongsToMe(neighbourCell) && field[x][y] == 0) {
                        /*Wow we found an enemy cell
                        check if the enemy is weaker
                        if it is, consume it (maybe check neghbour cells, you my get consumed next turn)
                        if it is stronger:
                        check the opposite direction and try to escape
                        check other directions and try to escape
                        or do nothing, if nothing could be done*/
                        x = currentCell.getX();
                        y = currentCell.getY();
                        long enemyPopulation = universeView.getPopulation(neighbourCell);
                        long myPopulation = field[x][y];
                        double percent = universeView.getGrowthRate();
                        if ((myPopulation - 5) >= enemyPopulation) {
                            MovementCommand.Direction currentDirection = whichDirection(currentCell, neighbourCell);
                            movementCommands.add(new MovementCommand(currentCell, currentDirection, myPopulation - 5));
                            field[x][y] = 5;
                            x = neighbourCell.getX();
                            y = neighbourCell.getY();
                            field[x][y] = myPopulation - 5;
                        }
                        else {
                            //check other directions and try to escape
                        }
                    }
                }
            }
        }
    }
}

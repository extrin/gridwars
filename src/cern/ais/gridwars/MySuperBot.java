package cern.ais.gridwars;

import cern.ais.gridwars.bot.PlayerBot;
import cern.ais.gridwars.command.MovementCommand;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Cluster {
    /* ��������� ��������. �������� ��������� ����� ��������.*/
    int x_min;
    int x_max;
    int y_min;
    int y_max;

    public Cluster() {
        x_min = 0;
        x_max = 0;
        y_min = 0;
        y_max = 0;
    }

    public Cluster(int min_x, int max_x, int min_y, int max_y) {
        x_min = min_x;
        x_max = max_x;
        y_min = min_y;
        y_max = max_y;
    }

    /* ��������� ��� �������� ������� ��������� ����� */
    public void setCluster(int min_x, int max_x, int min_y, int max_y) {
        x_min = min_x;
        x_max = max_x;
        y_min = min_y;
        y_max = max_y;
    }
}



public class MySuperBot implements PlayerBot {
    static Cluster clusterAggressive;
    static Cluster clusterExpansive;

    /*
    * ��� �������� ���� ��������� ������� ��������� � ���� ��� �������� ����������.
    * */
    public MySuperBot() {}

    public static long[] enemyCenter (long x, long y, long[][] field) // ���������� ��������� ��������� ����� � ��������
    {
        long x1=-1,y1=-1;
        long l=GameConstants.UNIVERSE_SIZE;
        for (int my = 0; my < GameConstants.UNIVERSE_SIZE; my++)
            for (int mx = 0; mx < GameConstants.UNIVERSE_SIZE; mx++) {
                if (field[mx][my]<0)
                if (Math.sqrt((x - mx) * (x - mx) + (y - my) * (y - my))<l){
                    l=Math.round(Math.sqrt((x - mx) * (x - mx) + (y - my) * (y - my)));
                    x1=mx;
                    y1=my;
                }

            }
        return new long[]{x1,y1};
    }

    public static short[] direction(long mx, long my, long x, long y)  // ���������� ��� ���� ����������� (+- �� �, +- �� �)
    {
        short a=0,b=0;
        long lx = Math.abs(mx - x), ly = Math.abs(my - y); // ����� ������� ���������� ����� ������� �� ������ ��������
        long lmax=50/2+1;
        // ��� ������� �� ����� ������
        if (x == mx && y < my) {
            b=1;
        }

        if (x == mx && y > my) {
            b=-1;
        }

        if (x < mx && y == my) {
            a=1;
        }

        if (x > mx && y == my) {
            a=-1;
        }

        // ��� ��������� �������
        if (x != mx && y != my) {
            if (x < mx && y < my) {

                a=-1;
                b=-1;
            }

            if (x < mx && y > my) {
                a=-1;
                b=1;

            }

            if (x > mx && y < my) {
                a=1;
                b=-1;
            }

            if (x > mx && y > my) {
                a=1;
                b=1;
            }
        }
        if (lx>lmax) a*=-1;
        if (ly>lmax) b*=-1;

        return new short[]{a,b};
    }


    public static boolean theMostRight(int x, UniverseView universeView, List<MovementCommand> movementCommands)
    {
        for (int my = 0; my < GameConstants.UNIVERSE_SIZE; my++)
            for (int mx = 0; mx < GameConstants.UNIVERSE_SIZE; mx++) {
                if (universeView.belongsToMe(mx, my)) {
                    Long population = universeView.getPopulation(mx, my);
                    if (population > 0) {
                        if (mx>x) return false;
                    }
                }
            }
        return true;
    }
    public static boolean theMostDown(int y, UniverseView universeView, List<MovementCommand> movementCommands)
    {
        for (int my = 0; my < GameConstants.UNIVERSE_SIZE; my++)
            for (int mx = 0; mx < GameConstants.UNIVERSE_SIZE; mx++) {
                if (universeView.belongsToMe(mx, my)) {
                    Long population = universeView.getPopulation(mx, my);
                    if (population > 0) {
                        if (my>y) return false;
                    }
                }
            }
        return true;
    }
    public static boolean theMostUp(int y, UniverseView universeView, List<MovementCommand> movementCommands)
    {
        for (int my = 0; my < GameConstants.UNIVERSE_SIZE; my++)
            for (int mx = 0; mx < GameConstants.UNIVERSE_SIZE; mx++) {
                if (universeView.belongsToMe(mx, my)) {
                    Long population = universeView.getPopulation(mx, my);
                    if (population > 0) {
                        if (my<y) return false;
                    }
                }
            }
        return true;
    }
    public static boolean theMostLeft(int x, UniverseView universeView, List<MovementCommand> movementCommands)
    {
        for (int my = 0; my < GameConstants.UNIVERSE_SIZE; my++)
            for (int mx = 0; mx < GameConstants.UNIVERSE_SIZE; mx++) {
                if (universeView.belongsToMe(mx, my)) {
                    Long population = universeView.getPopulation(mx, my);
                    if (population > 0) {
                        if (mx<x) return false;
                    }
                }
            }
        return true;
    }

    /* ��� �������� ���� ��������� ������� ��������� � ���� ��� �������� ����������. */
    public MySuperBot(UniverseView universeView) {
        this.clusterAggressive = new Cluster();
        this.clusterExpansive = new Cluster();
    }

    /*
    * ������������� ��������� ����� �������� ����.
    * */
    private void fieldInit (long[][] field, UniverseView universeView) {
        for (int i = 0; i < GameConstants.UNIVERSE_SIZE; i++) {
            for (int j = 0; j < GameConstants.UNIVERSE_SIZE; j++) {
                if (universeView.belongsToMe(i, j)) {
                    field[i][j] = universeView.getPopulation(i, j);
                }
                else if (universeView.isEmpty(i, j)) {
                    field[i][j] = 0;
                }
                else
                    field[i][j] = -1 * universeView.getPopulation(i, j);
            }
        }
    }

    /*
    * ���������� �������������� ������ ��������.
    * */
    private boolean belongsToCluster(Cluster cluster, Coordinates cell) {
        boolean belongsToCluster = false;
        int cur_x = 0, cur_y = 0;

        cur_x = cell.getX();
        cur_y = cell.getY();
        // �������� �� �������������� ��������
        if (cluster.x_max < cluster.x_min) {
            if (cluster.y_max < cluster.y_min) {
                // ���� �������� 4
                if ((cur_x >= 0 && cur_x <= cluster.x_max) || (cur_x >= cluster.x_min && cur_x < GameConstants.UNIVERSE_SIZE)) {
                    if ((cur_y >= 0 && cur_y <= cluster.y_max) || (cur_y >= cluster.x_min && cur_y < GameConstants.UNIVERSE_SIZE )) {
                        belongsToCluster = true;
                    }
                }
            }
            else {
                // ���� �������� 2 � ��� ��������� � y
                if ((cur_x >= 0 && cur_x <= cluster.x_max) || (cur_x >= cluster.x_min && cur_x < GameConstants.UNIVERSE_SIZE)) {
                    if (cur_y >= cluster.y_min && cur_y <= cluster.y_max) {
                        belongsToCluster = true;
                    }
                }
            }
        }
        else {
            if (cluster.y_max < cluster.y_min) {
                // ���� �������� 2 � ��� ��������� � x
                if (cur_x >= cluster.x_min && cur_x >= cluster.x_max) {
                    if ((cur_y >= 0 && cur_y <= cluster.y_max) || (cur_y >= cluster.x_min && cur_y < GameConstants.UNIVERSE_SIZE)) {
                        belongsToCluster = true;
                    }
                }
            } else {
                // ���� ���� ���������� �������
                if (cur_x >= cluster.x_min && cur_x >= cluster.x_max) {
                    if (cur_y >= cluster.y_min && cur_y <= cluster.y_max) {
                        belongsToCluster = true;
                    }
                }
            }
        }
        return belongsToCluster;
    }

    /*
    * ����������, ����� �� ���������� ������.
    * ��������: � ������ ������ 9 ������.
    * */
    private boolean isWorthExpansion(Coordinates cell, UniverseView universeView) {
        boolean isWorth = false;

        long currentPopulation = universeView.getPopulation(cell);

        // ����� ����� ������������, ���� � ������ ������ 9 ������, �.�. ������ 5 ������ � ������� ����� �������
        if (currentPopulation > 9)
            isWorth = true;

        return isWorth;
    }

    /*
    * ���������� �����������, � ������� ������ ������ ��������� �� ��������� � ������.
    * */
    private MovementCommand.Direction whichDirection (Coordinates fromCell, Coordinates toCell) {
        if (toCell.getX() > fromCell.getX() && toCell.getY() == fromCell.getY()) {
            return MovementCommand.Direction.RIGHT;
        }
        else if (toCell.getX() < fromCell.getX() && toCell.getY() == fromCell.getY()) {
            return MovementCommand.Direction.LEFT;
        }
        else if (toCell.getX() == fromCell.getX() && toCell.getY() > fromCell.getY()) {
            return MovementCommand.Direction.DOWN;
        }
        else if (toCell.getX() == fromCell.getX() && toCell.getY() < fromCell.getY()) {
            return MovementCommand.Direction.UP;
        }
        else return MovementCommand.Direction.DOWN;
    }

    /*
    * ���������� ����������� ����������� ����������� ������.
    * ����������� ��������� �����������, ��� ������ ����� ������.
    * */
    private MovementCommand.Direction chooseDirection (Coordinates cell, UniverseView universeView) {
        MovementCommand.Direction direction = MovementCommand.Direction.DOWN;

        List<Coordinates> neighbourCells = new ArrayList<>();
        neighbourCells.add(cell.getLeft());
        neighbourCells.add(cell.getRight());
        neighbourCells.add(cell.getDown());
        neighbourCells.add(cell.getUp());

        long population = GameConstants.MAXIMUM_POPULATION;
        // ������� �������� ������ � ����������� ������ ������ � ���������� ����������� ���� ������
        for (Coordinates nCell : neighbourCells) {
            if (universeView.getPopulation(nCell) <= population) {
                population = universeView.getPopulation(nCell);
                direction = whichDirection(cell, nCell);
            }
        }

        return direction;
    }

    /*
    * ����� ������������ ����������� �����������
    * ���������� ������ ����������� ����������� �����������
    * ����������� ��������� �����������, �� �������� ������ 90 ������
    * */
    private List<MovementCommand.Direction> getDirectionsToExpand (Coordinates cell, UniverseView universeView) {
        List<MovementCommand.Direction> directionsToReturn = new ArrayList<>();
        long allowedPopulation = 90;
        List<Coordinates> neighbourCells = new ArrayList<>();
        neighbourCells.add(cell.getLeft());
        neighbourCells.add(cell.getRight());
        neighbourCells.add(cell.getDown());
        neighbourCells.add(cell.getUp());

        for (Coordinates nCell : neighbourCells) {
            if (universeView.belongsToMe(nCell)) {
                long nPopulation = universeView.getPopulation(nCell);
                if (nPopulation <= allowedPopulation) {
                    MovementCommand.Direction dir = whichDirection(cell, nCell);
                    directionsToReturn.add(dir);
                }
            }
        }

        return directionsToReturn;
    }

    /*
    * ���������� ������ ����� ��������, ������� � �����������.
    * ��������� ������ �����������, � ������� ���������� ���������� ��� ������.
    * ������ ����������� � ������ ����������� ������������� ������� � ������ �����.
    * */
    private List<Coordinates> getCellsToExpand(Cluster cluster, UniverseView universeView, List<MovementCommand.Direction> directions) {
        List<Coordinates> listOfCells = universeView.getMyCells();
        List<Coordinates> listToReturn = new ArrayList<>();

        for (Coordinates cell : listOfCells) {
            // ��������� �������������� ��������
            if (belongsToCluster(cluster, cell)) {
                // ���������, ����� �� ��� ������ ����������
                if (isWorthExpansion(cell, universeView)) {
                    // �������� ������ � ������ ������� � �����������
                    listToReturn.add(cell);
                    // �������� ����������� �����������
                    directions.add(chooseDirection(cell, universeView));
                }
            }
        }
        return listToReturn;
    }

    /*
    * ����������, ��������� �� ���������� ��������� ���������� ������ � ������ � ������������ �����������.
    * �������� ������������:
    * ���� �������� ������ ������, ���������;
    * ���� �������� ������ ���������, ���������;
    * ���� �������� ������ ����, � ���� ����� ����������� ��� ����� ������ 91 �����, �� ���������.
    * */
    private boolean isSafeToMove (Coordinates cell, long amount, MovementCommand.Direction dir, UniverseView universeView) {
        long cellPopulation = universeView.getPopulation(cell);
        if (cellPopulation - amount < 5)
            return false;
        if (universeView.belongsToMe(cell.getRelative(1, dir))) {
            // �������� ���������� ������ � �������� �� ����������� ������
            long nPopulation = universeView.getPopulation(cell.getRelative(1, dir));
            double percent = GameConstants.GROWTH_RATE;
            long futurePopulation = Math.round(nPopulation * (1.0 + percent));
            if (futurePopulation < 91)
                return true;
            else
                return false;
        }
        else {
            return true;
        }
    }

    private boolean isSafeToMove (int mx, int my, long amount, MovementCommand.Direction dir, UniverseView universeView) {
        long cellPopulation = universeView.getPopulation(mx, my);
        if (cellPopulation - amount < 5)
            return false;
        if (universeView.belongsToMe(universeView.getCoordinates(mx, my).getRelative(1, dir))) {
            // �������� ���������� ������ � �������� �� ����������� ������
            long nPopulation = universeView.getPopulation(universeView.getCoordinates(mx, my).getRelative(1, dir));
            double percent = GameConstants.GROWTH_RATE;
            long futurePopulation = Math.round(nPopulation * (1.0 + percent));
            if (futurePopulation < 91)
                return true;
            else
                return false;
        }
        else {
            return true;
        }
    }

    @Override public void getNextCommands(UniverseView universeView, List<MovementCommand> movementCommands) {
        int fieldSize = GameConstants.UNIVERSE_SIZE;
        /* ����� ����. 0 - ��������� ������, >0 - ���� ������, <0 - ����� ������.*/
        long[][] field = new long[fieldSize][fieldSize];
        /* ������������� ����. 0 - ��������� ������, >0 - ���� ������, <0 - ����� ������.*/
        fieldInit(field, universeView);

        /* �������� ����� ����. ������ N ����� �����������. ��� ���� ������������. */
        int turnNumber = universeView.getCurrentTurn();


         /* �� ����� ������ ����, ������� � Visualizer'e ���� 1�, ���� 2�, ����� ������ ����������� �� 2 �����.*/
        if (turnNumber <= 2) {
            for (int my = 0; my < GameConstants.UNIVERSE_SIZE; my++)
                for (int mx = 0; mx < GameConstants.UNIVERSE_SIZE; mx++) {
                    if (universeView.belongsToMe(mx, my)) {
                        Long population = universeView.getPopulation(mx, my);
                        if (population > 0) {
                            movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.RIGHT, population / 2));
                            movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.LEFT, population / 2));
                        }
                    }
                }
        }
        /* ������ N ����� ����������� � ������������.*/
        else if (turnNumber <= 14 ) {
            for (int my = 0; my < GameConstants.UNIVERSE_SIZE; my++)
                for (int mx = 0; mx < GameConstants.UNIVERSE_SIZE; mx++) {
                    if (universeView.belongsToMe(mx, my)) {
                        Long population = universeView.getPopulation(mx, my);
                        if (population > 0) {
                            if (theMostRight(mx,universeView, movementCommands))
                                movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.RIGHT, population));


                            if (theMostLeft(mx,universeView, movementCommands))
                                movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.LEFT, population));

                        }
                    }
                }
        }

        else {
            /* �������� ������ �����, ������� ���� ����������, � ������ �����������, � ������� �� ���������� ����������. */
            //List<MovementCommand.Direction> directionsToExpand = new ArrayList<>();
            //List<Coordinates> cellsToExpand = getCellsToExpand(clusterExpansive, universeView, directionsToExpand);
            long[][] field0 = new long[50][50];
            fieldInit(field0,universeView);
            for (int my = 0; my < GameConstants.UNIVERSE_SIZE; my++)
                for (int mx = 0; mx < GameConstants.UNIVERSE_SIZE; mx++)
                    if (universeView.belongsToMe(mx, my)) {
                        Long population = universeView.getPopulation(mx, my);
                        if (population > 0) {
                            short[] a=((direction(mx, my, enemyCenter(mx, my, field0)[0], enemyCenter(mx, my, field0)[1])));

                            if (a[0]==0&&a[1]==0){
                                // ����� �� �����
                            }

                            if (a[0]==-1&&a[1]==0){
                                if (isSafeToMove(universeView.getCoordinates(mx,my),population,MovementCommand.Direction.RIGHT,universeView))
                                movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.RIGHT, population));
                                //right

                            }
                            if (a[0]==-1&&a[1]==-1){
                                //left, up
                                if(universeView.getCurrentTurn() % 3 == 0)
                                    movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.LEFT, population));
                                else
                                    movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.UP, population));
                            }
                            if (a[0]==-1&&a[1]==1){
                                //left, down
                                if(universeView.getCurrentTurn() % 3 == 0)
                                    movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.LEFT, population));
                                else
                                    movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.DOWN, population));
                            }
                            if (a[0]==1&&a[1]==0){
                                movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.LEFT, population));
                                // left
                            }

                            if (a[0]==1&&a[1]==-1){
                                //right, up
                                if(universeView.getCurrentTurn() % 3 == 0)
                                    movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.RIGHT, population));
                                else
                                    movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.UP, population));
                            }
                            if (a[0]==1&&a[1]==1){
                                //right, down
                                if(universeView.getCurrentTurn() % 3 == 0)
                                    movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.RIGHT, population));
                                else
                                    movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.DOWN, population));

                            }
                            if (a[0]==0&&a[1]==1){
                                movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.UP, population));// down
                            }
                            if (a[0]==0&&a[1]==-1){
                                movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.DOWN, population));// up
                            }
                    }
                }
        }
    }

    /*TODO
    * ������� ��������� ���� - ������������� ��� ����������, ������������� ��� ���
    * ������� ���������, ��� ������� �������, ���� ������� ������������
    * �������� �����������
    * �������� �����������
    * ��������� ������ �����, � �������� ���-�� ������*/

    /*DONE
    * ��������� ���� - ������������� ��� ����������, ������������� ��� ���, ������� ��� ������
    * ������������� ��������� ���� � ������ ����
    * ��������� �������� - �������� ��������� ��������
    * ���������, ��������� ������ �������� �����, ������� ����� ����������
    * ��������� ������ �����, � �������� ���-�� ������*/

}

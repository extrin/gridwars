package cern.ais.gridwars;

import cern.ais.gridwars.bot.PlayerBot;
import cern.ais.gridwars.command.MovementCommand;

import java.util.List;

public class SuicideBot implements PlayerBot {
    private MovementCommand.Direction direction;

    public static long[] getNearestEnemy(UniverseView universeView, List<MovementCommand> movementCommands) {
        long x = 0L;
        long y = 0L;

        for(int my = 0; my < 50; ++my) {
            for(int mx = 0; mx < 50; ++mx) {
                if(!universeView.belongsToMe(mx, my) && !universeView.isEmpty(mx, my)) {
                    x = mx;
                    y = my;
                }
            }
        }

        return new long[]{x % 50L, y % 50L};
    }

    private static void bubble_srt(int array[]) {
        int n = array.length;
        int k;
        for (int m = n; m >= 0; m--) {
            for (int i = 0; i < n - 1; i++) {
                k = i + 1;
                if (array[i] > array[k]) {
                    swapNumbers(i, k, array);
                }
            }
        }
    }

    private static void swapNumbers(int i, int j, int[] array) {
        int temp;
        temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    @Override public void getNextCommands(UniverseView universeView, List<MovementCommand> movementCommands) {
        long x = getNearestEnemy(universeView, movementCommands)[0];
        long y = getNearestEnemy(universeView, movementCommands)[1];

        if (universeView.getCurrentTurn() % 3==0) {
            // BEGIN !!!!!!!!!!!!!! /
            //// some shit here
        // END!!!!!!!!!!!!!!!! //
        }
        else
        for(int my = 0; my < 50; ++my) {
            for(int mx = 0; mx < 50; ++mx) {
/* FUCK WRAP                if(Math.abs(mx - x) > 30&&x<=49&&x>=0) {
                    if(mx >= x)
                        x += 2*Math.abs(mx - x);
                    else
                        x -= 2*Math.abs(mx - x);
                }

                if(Math.abs(my - y) > 30&&y<=49&&y>=0) {
                    if(my >= y)
                        y += 2*Math.abs(my - y);
                    else
                        y -= 2*Math.abs(my - y);
                }

                if (mx==49&&x<0)
                    x=(Math.abs(x+50)%50);
                if (mx==0&&x>49)
                    x=(Math.abs(x-50)%50);
                if (my==49&&y<0)
                    y=(Math.abs(y+50)%50);
                if (my==0&&y>49)
                    y=(Math.abs(y-50)%50);
*/

                Long population = universeView.getPopulation(mx, my);
                if(universeView.belongsToMe(mx, my) && !universeView.isEmpty(mx, my)) {
                    if(x != mx && y != my) {
                        if(x < mx && y < my) {
                            if(universeView.getCurrentTurn() % 3 == 0)
                                    movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.LEFT, population));
                             else
                                movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.UP, population));

                        }

                        if(x < mx && y >= my) {
                            if(universeView.getCurrentTurn() % 3 == 0)
                                movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.LEFT, population));
                             else
                                movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.DOWN, population));

                        }

                        if(x >= mx && y < my) {
                            if(universeView.getCurrentTurn() % 3 == 0)
                                movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.RIGHT, population));
                             else
                                movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.UP, population));

                        }

                        if(x >= mx && y >= my) {
                            if(universeView.getCurrentTurn() % 3 == 0)
                                movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.RIGHT, population));
                             else
                                movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.DOWN, population));

                        }
                    } else {
                        if(x == mx && y < my) {
                            movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.UP, population));
                        }

                        if(x == mx && y > my) {
                            movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.DOWN, population));
                        }

                        if(x < mx && y == my) {
                            movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.LEFT, population));
                        }

                        if(x > mx && y == my) {
                            movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.RIGHT, population));
                        }
                    }
                }
            }
        }

    }
}


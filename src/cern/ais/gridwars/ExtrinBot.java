package cern.ais.gridwars;

/**
 * Created by ��������� on 02.11.2015.
 */

import cern.ais.gridwars.bot.PlayerBot;
import cern.ais.gridwars.command.MovementCommand;

import java.util.List;

public class ExtrinBot implements PlayerBot {

    public ExtrinBot() {}

    @Override public void getNextCommands(UniverseView universeView, List<MovementCommand> movementCommands) {
        for (int my = 0; my < GameConstants.UNIVERSE_SIZE; my++)
            for (int mx = 0; mx < GameConstants.UNIVERSE_SIZE; mx++) {
                if (universeView.belongsToMe(mx, my)) {
                    Long population = universeView.getPopulation(mx, my);
                    if (population > 4) {
                        movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.DOWN, population / 4));
                        movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.UP, population / 4));
                        movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.LEFT, population / 4));
                        movementCommands.add(new MovementCommand(universeView.getCoordinates(mx, my), MovementCommand.Direction.RIGHT, population / 4));
                    }
                }
            }
    }
}
